ESP32 Dishwasher
================

This project came about because the controller for my Siemens SE64E336EU dishwasher died abruptly. I wasn't able to find a direct replacement, and all of the ones that seemed to be compatible cost way more than I paid for the dishwasher in the first place.

Instead of throwing out the otherwise functional dishwasher, I decided to make my own controller for it using an Espressif microcontroller.


Attempts
--------

I started using a combo board with 4 relays and an ESP-12F module, but it would constantly shut itself off after a small number of actions. (either toggling relays or switching the triacs I had wired up to the additional I/O pins) I was also rather pin-constrained on this module, since it didn't have enough functional I/O pins to fully connect all of thee triacs and sensors I wanted to attach.

I then tried using an ESP32-CAM board I had lying around, and while it solved some of the issues, it still ended up pin constrained and unstable. I don't remember what my final verdict on this one was, but I ended up deciding to purchase a separate board to control the project.

That's how I finally ended up with the ESP32-based Lolin S2 Mini as the core of this project. It has enough pins to comfortably connect everything I need, and I was able to get it stable enough. The big stumbling block with this one was support; it's difficult to get it to flash correctly, and I wasn't able to get either Rust or Micropython working consistently on it. (and I couldn't get CircuitPython working at all)

Current Status
--------------

As of right now, this is a C++-based Arduino project running on a Lolin S2 Mini. It connects to MQTT to listen for commands and publish status information. It uses TaskScheduler for concurrency.

The LED on the S2 Mini is used to indicate connection status: it will blink slowly when disconnected from WiFi, faster once connected to WiFi but still connecting to MQTT, and then stay mostly on with very short off pulses once connected to MQTT. (I added the short off pulses as a visible sign that the firmware hasn't crashed)

It currently has the ability to run a full wash cycle correctly, as well as doing a rinse-only cycle, or various other test commands. (see `src/DishwasherProgram.hpp` for the full list of commands) It listens for commands at `devices/dishwasher/command` in MQTT, and publishes status at siblings of that key.


Remaining Work
--------------

The water softener system in the dishwasher still needs work in order to function correctly; right now, I have yet to correctly figure out how to run the regeneration cycle on it, so it's not effectively softening the water. This is leading to lots of mineral build-up.
