#include <Arduino.h>

#include "Dishwasher.hpp"


#define PIN_DEBOUNCE_MILLIS 30

// Pins
#ifdef LOLIN_S2_MINI

int ledPin = 15;                     // outputs PWM signal at boot, strapping pin

int waterInletValvePin = 13;
int drainMotorPin = 21;              // U4

int heaterPin = 16;                  // U1

int mainMotorStartupPin = 17;        // U2
int mainMotorRunPin = 18;            // U3

int waterSoftenerRegenValvePin = 14; // outputs PWM signal at boot

int soapDispenserActuatorPin = 12;   // boot fails if pulled high, strapping pin

int enableNtcPin = 33;
int ntcSensePin = 36;

//int startButtonPin = 39;             // input only (external button)
int startButtonPin = 0;              // outputs PWM signal at boot, must be LOW to enter flashing mode (built-in 0 button)
int doorSwitchPin = 34;              // input only (X2.1)
int waterSoftenerSensorPin = 35;     // input only (X6.5? X6.6?)
int rinseAidSensorPin = 39;          // input only (X5.1? X5.2?)

#else

int ledPin = 33;

int waterInletValvePin = 2;
int drainMotorPin = 12;

int heaterPin = 16;

#  if ENABLE_SERIAL
int mainMotorStartupPin = 14; // U2; outputs PWM signal at boot
int mainMotorRunPin = 13;     // U3
#  else
int mainMotorStartupPin = 3; // U2; RX - HIGH at boot
int mainMotorRunPin = 1;     // U3; TX - outputs debug at boot, and serial after if ENABLE_SERIAL
#  endif

#  if ENABLE_ADC
int waterSoftenerRegenValvePin = 1; // TX - outputs debug at boot, and serial after if ENABLE_SERIAL

int soapDispenserActuatorPin = 3;   // RX - HIGH at boot

int enableNtcPin = 15;              // outputs PWM signal at boot
int ntcSensePin = 4;
#  else
int waterSoftenerRegenValvePin = 15; // outputs PWM signal at boot

int soapDispenserActuatorPin = 4;

int enableNtcPin = -1;
int ntcSensePin = -1;
#  endif

int startButtonPin = 0;
int doorSwitchPin = -1;              // FIXME: determine pin for this (X2.1)
int waterSoftenerSensorPin = -1;     // FIXME: determine pin for this (X6.5? X6.6?)
int rinseAidSensorPin = -1;          // FIXME: determine pin for this (X5.1? X5.2?)

#endif

// Status Request objects
StatusRequest startButtonStatusRequest;
StatusRequest doorClosedStatusRequest;
StatusRequest waterSoftenerStatusRequest;
StatusRequest rinseAidStatusRequest;

// Interrupt service routines
void IRAM_ATTR onStartButtonChanged();
void IRAM_ATTR onDoorClosedChanged();
void IRAM_ATTR onWaterSoftenerLowChanged();
void IRAM_ATTR onRinseAidLowChanged();

// State
volatile int startButtonState = 0;
volatile int doorClosedState = 0;
volatile int waterSoftenerState = 0;
volatile int rinseAidState = 0;



#define TURN_ON(PIN) \
	LOG("Turning on pin %u", PIN); \
	digitalWrite(PIN, HIGH);

#define TURN_OFF(PIN) \
	LOG("Turning off pin %u", PIN); \
	digitalWrite(PIN, LOW);


////////
// Tasks
int sampleTemperatureAccum = 0;
void sampleTemperatureThread();
void sampleTemperatureFinish();
Task tSampleTemperature(
	SAMPLE_TEMPERATURE_INTERVAL * TASK_MILLISECOND,
	SAMPLE_TEMPERATURE_ITERATIONS,
	sampleTemperatureThread,
	TS,
	false,
	NULL,
	sampleTemperatureFinish
);
void sampleTemperatureThread() {
	sampleTemperatureAccum += analogRead(ntcSensePin);
}
void sampleTemperatureFinish() {
	//TURN_OFF(enableNtcPin);
	digitalWrite(enableNtcPin, LOW);
	LOG("Completed temperature sampling; average temp: %f", getWaterTemperature());
}

#define RELEASE_DETERGENT_ITERATIONS 2
void releaseDetergentThread();
void releaseDetergentFinish();
Task tReleaseDetergent(
	2000 * TASK_MILLISECOND,
	RELEASE_DETERGENT_ITERATIONS,
	releaseDetergentThread,
	TS,
	false,
	NULL,
	releaseDetergentFinish
);
void releaseDetergentThread() {
	unsigned long iteration = tReleaseDetergent.getRunCounter();
	switch(iteration) {
		case 1:
			TURN_ON(soapDispenserActuatorPin);
			tReleaseDetergent.setInterval(40 * TASK_SECOND);
			break;
		case 2:
			TURN_OFF(soapDispenserActuatorPin);
			tReleaseDetergent.setInterval(80 * TASK_SECOND);
			break;
		default:
			LOG("Unrecognized iteration %d", iteration);
			break;
	}
}
void releaseDetergentFinish() {
	LOG("Finished releasing detergent.");
}

#define RELEASE_RINSE_AID_ITERATIONS 2
void releaseRinseAidThread();
void releaseRinseAidFinish();
Task tReleaseRinseAid(
	2000 * TASK_MILLISECOND,
	RELEASE_RINSE_AID_ITERATIONS,
	releaseRinseAidThread,
	TS,
	false,
	NULL,
	releaseRinseAidFinish
);
void releaseRinseAidThread() {
	unsigned long iteration = tReleaseRinseAid.getRunCounter();
	switch(iteration) {
		case 1:
			TURN_ON(soapDispenserActuatorPin);
			tReleaseRinseAid.setInterval(40 * TASK_SECOND);
			break;
		case 2:
			TURN_OFF(soapDispenserActuatorPin);
			tReleaseRinseAid.setInterval(80 * TASK_SECOND);
			break;
		default:
			LOG("Unrecognized iteration %d", iteration);
			break;
	}
}
void releaseRinseAidFinish() {
	LOG("Finished releasing rinse aid.");
}

#define START_MAIN_MOTOR_ITERATIONS 3
void startMainMotorThread();
void startMainMotorFinish();
Task tStartMainMotor(
	1000 * TASK_MILLISECOND,
	START_MAIN_MOTOR_ITERATIONS,
	startMainMotorThread,
	TS,
	false,
	NULL,
	startMainMotorFinish
);
void startMainMotorThread() {
	unsigned long iteration = tStartMainMotor.getRunCounter();
	switch(iteration) {
		case 1:
			TURN_OFF(mainMotorStartupPin);
			tStartMainMotor.setInterval(1000 * TASK_MILLISECOND);
			break;
		case 2:
			TURN_OFF(mainMotorRunPin);
			tStartMainMotor.setInterval(500 * TASK_MILLISECOND);
			break;
		case 3:
			TURN_ON(mainMotorStartupPin);
			break;
		default:
			LOG("Unrecognized iteration %d", iteration);
			break;
	}
}
void startMainMotorFinish() {
	LOG("Finished starting main motor.");
}


void setupPins() {
	pinMode(ledPin, OUTPUT);

	pinMode(waterInletValvePin, OUTPUT);
	pinMode(drainMotorPin, OUTPUT);

	pinMode(heaterPin, OUTPUT);

	pinMode(mainMotorStartupPin, OUTPUT);
	pinMode(mainMotorRunPin, OUTPUT);

#if ENABLE_WATER_SOFTENER_REGEN
	pinMode(waterSoftenerRegenValvePin, OUTPUT);
#endif

#if ENABLE_SOAP_DISPENSER
	pinMode(soapDispenserActuatorPin, OUTPUT);
#endif

#if ENABLE_ADC
	pinMode(enableNtcPin, OUTPUT);
	pinMode(ntcSensePin, INPUT_PULLUP);
#endif

	if (startButtonPin >= 0) {
		startButtonStatusRequest.setWaiting();
		pinMode(startButtonPin, INPUT_PULLUP);
		attachInterrupt(startButtonPin, onStartButtonChanged, CHANGE);
	}
	if (doorSwitchPin >= 0) {
		doorClosedStatusRequest.setWaiting();
		pinMode(doorSwitchPin, INPUT_PULLUP);
		attachInterrupt(doorSwitchPin, onDoorClosedChanged, CHANGE);
	}
	if (waterSoftenerSensorPin >= 0) {
		waterSoftenerStatusRequest.setWaiting();
		pinMode(waterSoftenerSensorPin, INPUT_PULLUP);
		attachInterrupt(waterSoftenerSensorPin, onWaterSoftenerLowChanged, CHANGE);
	}
	if (rinseAidSensorPin >= 0) {
		rinseAidStatusRequest.setWaiting();
		pinMode(rinseAidSensorPin, INPUT_PULLUP);
		attachInterrupt(rinseAidSensorPin, onRinseAidLowChanged, CHANGE);
	}
}

void resetOutputs() {
	LOG("[Dishwasher] Resetting outputs...");
	// Relay board inverts its inputs
	TURN_ON(heaterPin);
	TURN_ON(mainMotorStartupPin);
	TURN_ON(mainMotorRunPin);
	TURN_ON(drainMotorPin);

	TURN_OFF(waterInletValvePin);
#if ENABLE_WATER_SOFTENER_REGEN
	TURN_OFF(waterSoftenerRegenValvePin);
#endif
#if ENABLE_SOAP_DISPENSER
	TURN_OFF(soapDispenserActuatorPin);
#endif
#if ENABLE_ADC
	TURN_OFF(enableNtcPin);
#endif
}

void turnOnPin(uint8_t pin) {
	LOG("Turning on pin %u", pin);
	TURN_ON(pin);
}

void turnOffPin(uint8_t pin) {
	LOG("Turning off pin %u", pin);
	TURN_OFF(pin);
}

void turnOnLED() {
	//TURN_ON(ledPin);
	digitalWrite(ledPin, HIGH);
}

void turnOffLED() {
	//TURN_OFF(ledPin);
	digitalWrite(ledPin, LOW);
}

void startFill() {
	TURN_ON(waterInletValvePin);
}

void stopFill() {
	TURN_OFF(waterInletValvePin);
}

void startDrain() {
	TURN_OFF(drainMotorPin);
}

void stopDrain() {
	TURN_ON(drainMotorPin);
}

void startHeater() {
	TURN_OFF(heaterPin);
}

void stopHeater() {
	TURN_ON(heaterPin);
}

StatusRequest* startMainMotor() {
	tStartMainMotor.setIterations(START_MAIN_MOTOR_ITERATIONS);
	tStartMainMotor.restart();
	return tStartMainMotor.getInternalStatusRequest();
}

void stopMainMotor() {
	TURN_ON(mainMotorRunPin);
}

void startWaterSoftenerRegen() {
	TURN_ON(waterSoftenerRegenValvePin);
}

void stopWaterSoftenerRegen() {
	TURN_OFF(waterSoftenerRegenValvePin);
}

StatusRequest* releaseDetergent() {
	tReleaseDetergent.setIterations(RELEASE_DETERGENT_ITERATIONS);
	tReleaseDetergent.restart();
	return tReleaseDetergent.getInternalStatusRequest();
}

StatusRequest* releaseRinseAid() {
	tReleaseRinseAid.setIterations(RELEASE_RINSE_AID_ITERATIONS);
	tReleaseRinseAid.restart();
	return tReleaseRinseAid.getInternalStatusRequest();
}

StatusRequest* readWaterTemperature() {
	sampleTemperatureAccum = 0;
	//TURN_ON(enableNtcPin);
	digitalWrite(enableNtcPin, HIGH);
	tSampleTemperature.setIterations(SAMPLE_TEMPERATURE_ITERATIONS);
	tSampleTemperature.restart();
	return tSampleTemperature.getInternalStatusRequest();
}

float getWaterTemperature() {
	return sampleTemperatureAccum / SAMPLE_TEMPERATURE_ITERATIONS;
}

bool isStartButtonPressed() {
	return startButtonState == 0;
}

bool isDoorClosed() {
	return doorClosedState == 0;
}

bool isWaterSoftenerLow() {
	return waterSoftenerState == 0;
}

bool isRinseAidLow() {
	return rinseAidState == 0;
}

StatusRequest* getStartButtonStatusRequest() {
	return &startButtonStatusRequest;
}

StatusRequest* getDoorClosedStatusRequest() {
	return &doorClosedStatusRequest;
}

StatusRequest* getWaterSoftenerStatusRequest() {
	return &waterSoftenerStatusRequest;
}

StatusRequest* getRinseAidStatusRequest() {
	return &rinseAidStatusRequest;
}

// Debounce variables
unsigned long currentTime = 0;
unsigned long lastStartButtonTime = 0;
unsigned long lastDoorClosedTime = 0;
unsigned long lastWaterSoftenerTime = 0;
unsigned long lastRinseAidTime = 0;

void IRAM_ATTR onStartButtonChanged() {
	currentTime = millis();
	LOG("onStartButtonChanged: %d ms since last press", currentTime - lastStartButtonTime);
	if((currentTime - lastStartButtonTime) > PIN_DEBOUNCE_MILLIS) {
		LOG("passed debounce check; triggering startButtonStatusRequest");
		startButtonState = digitalRead(startButtonPin);
		startButtonStatusRequest.signal();
		lastStartButtonTime = currentTime;
		startButtonStatusRequest.setWaiting();
	}
}

void IRAM_ATTR onDoorClosedChanged() {
	currentTime = millis();
	if(currentTime - lastDoorClosedTime > PIN_DEBOUNCE_MILLIS) {
		doorClosedState = digitalRead(doorSwitchPin);
		doorClosedStatusRequest.signal();
		lastDoorClosedTime = currentTime;
	}
}

void IRAM_ATTR onWaterSoftenerLowChanged() {
	currentTime = millis();
	if(currentTime - lastWaterSoftenerTime > PIN_DEBOUNCE_MILLIS) {
		waterSoftenerState = digitalRead(waterSoftenerSensorPin);
		waterSoftenerStatusRequest.signal();
		lastWaterSoftenerTime = currentTime;
	}
}

void IRAM_ATTR onRinseAidLowChanged() {
	currentTime = millis();
	if(currentTime - lastRinseAidTime > PIN_DEBOUNCE_MILLIS) {
		rinseAidState = digitalRead(rinseAidSensorPin);
		rinseAidStatusRequest.signal();
		lastRinseAidTime = currentTime;
	}
}
