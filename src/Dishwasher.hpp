#ifndef __DISHWASHER_HPP__
#define __DISHWASHER_HPP__

#include "config.hpp"
#include "log.hpp"
#include "scheduler.hpp"

#define SAMPLE_TEMPERATURE_INTERVAL 10
#define SAMPLE_TEMPERATURE_ITERATIONS 8


void setupPins();
void resetOutputs();

// Controls
void turnOnPin(uint8_t pin);
void turnOffPin(uint8_t pin);

void turnOnLED();
void turnOffLED();

void startFill();
void stopFill();
void startDrain();
void stopDrain();

void startHeater();
void stopHeater();

StatusRequest* startMainMotor();
void stopMainMotor();

void startWaterSoftenerRegen();
void stopWaterSoftenerRegen();

StatusRequest* releaseDetergent();
StatusRequest* releaseRinseAid();

// Status checks
StatusRequest* readWaterTemperature();
float getWaterTemperature();

bool isStartButtonPressed();
bool isDoorClosed();
bool isWaterSoftenerLow();
bool isRinseAidLow();

// Status Request objects
StatusRequest* getStartButtonStatusRequest();
StatusRequest* getDoorClosedStatusRequest();
StatusRequest* getWaterSoftenerStatusRequest();
StatusRequest* getRinseAidStatusRequest();

#endif // __DISHWASHER_HPP__
