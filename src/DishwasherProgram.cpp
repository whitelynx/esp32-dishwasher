#include <AsyncMqttClient.h>

#include "config.hpp"
#include "Dishwasher.hpp"
#include "DishwasherProgram.hpp"


const char *dishwasherCommandNames[] = {
	"NONE",
	"NORMAL_PROGRAM",
	"RINSE_PROGRAM",
	"TEST_MAIN_MOTOR",
	"TEST_HEATER",
	"TEST_PINS",
	"TEST_SOAP_DISPENSER",
	"START_MAIN_MOTOR",
	"STOP_MAIN_MOTOR",
	"TURN_PIN_ON",
	"TURN_PIN_OFF"
};

const char *cycleTypeNames[] = {
	"PREWASH",
	"MAIN_WASH",
	"FINAL_WASH",
	"FINAL_WASH_DELICATE",
	"FINAL_WASH_SANITIZE",
	"DRY"
};

extern AsyncMqttClient mqttClient;

DishwasherCommand lastCommand = DishwasherCommand::NONE;

#define FILL_DURATION_SECONDS 60
#define DRAIN_DURATION_SECONDS 20
#define HEAT_DURATION_SECONDS 360

////////
// Cycle
#define CYCLE_ITERATIONS 10
void cycleThread();
void cycleFinish();

Task tCycle(
	TASK_SECOND,
	CYCLE_ITERATIONS,
	cycleThread,
	TS,
	false,
	NULL,
	cycleFinish
);

CycleType currentCycleType;
unsigned long currentCycleStartTime;

const float cycleTemperatures[] = {
	50.0, // CycleType::PREWASH

	50.0, // CycleType::MAIN_WASH

	55.0, // CycleType::FINAL_WASH
	50.0, // CycleType::FINAL_WASH_DELICATE
	// sanitize should be at least 65.5556°C (150°F); some use temps in the range 63.3333°C-75°C (146°F-167°F)
	65.6, // CycleType::FINAL_WASH_SANITIZE

	65.0 // CycleType::DRY
};

unsigned long cycleLengths[] = {
	7 * 60 * 1000, // CycleType::PREWASH - 6-8 min.
	40 * 60 * 1000, // CycleType::MAIN_WASH - 20-60 min.
	40 * 60 * 1000, // CycleType::FINAL_WASH - 20-60 min.
	20 * 60 * 1000, // CycleType::FINAL_WASH_DELICATE - 20-60 min. (shorter)
	60 * 60 * 1000, // CycleType::FINAL_WASH_SANITIZE - 20-60 min. (longer)
	15 * 60 * 1000 // CycleType::DRY
};

void cycleThread() {
	unsigned long remainingIterations = tCycle.getIterations();
	LOG("Iterations remaining: %d", remainingIterations);
	switch(remainingIterations) {
		case 9:
			currentCycleStartTime = millis();
			if(currentCycleType != CycleType::DRY) {
				mqttClient.publish(MQTT_TOPIC_ROOT "step", 1, true, "fill with water");
				startFill();
				tCycle.setInterval((FILL_DURATION_SECONDS - 5) * TASK_SECOND);
			} else {
				tCycle.setInterval(TASK_SECOND);
			}
			break;
		case 8:
			if(currentCycleType != CycleType::DRY) {
				mqttClient.publish(MQTT_TOPIC_ROOT "step", 1, true, "fill with water (+heating)");
				startHeater();
			}
			tCycle.setInterval(5 * TASK_SECOND);
			break;
		case 7:
			if(currentCycleType != CycleType::DRY) {
				mqttClient.publish(MQTT_TOPIC_ROOT "step", 1, true, "(heating)");
				stopFill();
			}
			tCycle.setInterval(1 * TASK_SECOND);
			break;

		case 6:
			if(currentCycleType != CycleType::DRY) {
				mqttClient.publish(MQTT_TOPIC_ROOT "step", 1, true, "running (+heating)");
				tCycle.waitForDelayed(
					startMainMotor(),
					(HEAT_DURATION_SECONDS - 6) * TASK_SECOND,
					tCycle.getIterations()
				);
			}
			//TODO: Wait for water to reach correct temperature.
			//cycleTemperatures[int8_t(currentCycleType)]
			break;
		case 5:
			if(currentCycleType != CycleType::DRY) {
				mqttClient.publish(MQTT_TOPIC_ROOT "step", 1, true, "running");
				stopHeater();
			}
			tCycle.setInterval(
				(
					cycleLengths[int8_t(currentCycleType)]
					- (millis() - currentCycleStartTime)
				) * TASK_MILLISECOND
			);
			break;
		case 4:
			mqttClient.publish(MQTT_TOPIC_ROOT "step", 1, true, "waiting");
			stopMainMotor();
			tCycle.setInterval(1 * TASK_SECOND);
			break;

		//FIXME: How does water softener regen work, and where in the cycle does it go?
		//startWaterSoftenerRegen();
		//stopWaterSoftenerRegen();

		case 3:
			switch(currentCycleType) {
				case CycleType::MAIN_WASH:
					mqttClient.publish(MQTT_TOPIC_ROOT "step", 1, true, "releasing detergent");
					releaseDetergent();
					break;
				case CycleType::FINAL_WASH:
				case CycleType::FINAL_WASH_DELICATE:
				case CycleType::FINAL_WASH_SANITIZE:
					mqttClient.publish(MQTT_TOPIC_ROOT "step", 1, true, "releasing rinse aid");
					releaseRinseAid();
					break;
				default:
					// Don't release detergent or rinse aid.
					break;
			}
			break;

		case 2:
			if(currentCycleType != CycleType::DRY) {
				mqttClient.publish(MQTT_TOPIC_ROOT "step", 1, true, "draining");
				startDrain();
			}
			tCycle.setInterval(DRAIN_DURATION_SECONDS * TASK_SECOND);
			break;
		case 1:
			if(currentCycleType != CycleType::DRY) {
				mqttClient.publish(MQTT_TOPIC_ROOT "step", 1, true, "waiting");
				stopDrain();
			}
			tCycle.setInterval(1 * TASK_SECOND);
			break;
		case 0:
			mqttClient.publish(MQTT_TOPIC_ROOT "step", 1, true, "");
			LOG("Last iteration.");
			break;

		default:
			LOG("Unrecognized number of remaining iterations %d", remainingIterations);
			break;
	}
}

void cycleFinish() {
	LOG("Finished.");
    lastCommand = DishwasherCommand::NONE;
}

StatusRequest* runCycle(CycleType cycleType) {
	if(tCycle.isEnabled()) {
		LOG("Ignoring runCycle call; task already enabled.");
	} else {
		const char* cycleTypeName = cycleTypeNames[int8_t(cycleType)];
		LOG("Running cycle %s", cycleTypeName);
		mqttClient.publish(MQTT_TOPIC_ROOT "cycle", 1, true, cycleTypeName);
		currentCycleType = cycleType;
		tCycle.setIterations(CYCLE_ITERATIONS);
		tCycle.restartDelayed(2 * TASK_SECOND);
	}
	return tCycle.getInternalStatusRequest();
}


////////
// Drain
#define DRAIN_ITERATIONS 3
void drainThread();
void drainFinish();

Task tDrain(
	TASK_SECOND,
	DRAIN_ITERATIONS,
	drainThread,
	TS,
	false,
	NULL,
	drainFinish
);

void drainThread() {
	unsigned long remainingIterations = tDrain.getIterations();
	LOG("Iterations remaining: %d", remainingIterations);
	switch(remainingIterations) {
		case 2:
			startDrain();
			tDrain.setInterval(DRAIN_DURATION_SECONDS * TASK_SECOND);
			break;
		case 1:
			stopDrain();
			tDrain.setInterval(1 * TASK_SECOND);
			break;
		case 0:
			LOG("Last iteration.");
			break;

		default:
			LOG("Unrecognized number of remaining iterations %d", remainingIterations);
			break;
	}
}

void drainFinish() {
	LOG("Finished.");
    lastCommand = DishwasherCommand::NONE;
}


/////////////////
// Normal program
#define NORMAL_PROGRAM_ITERATIONS 6
void normalProgramThread();
void normalProgramFinish();

Task tNormalProgram(
	TASK_SECOND,
	NORMAL_PROGRAM_ITERATIONS,
	normalProgramThread,
	TS,
	false,
	NULL,
	normalProgramFinish
);

void normalProgramThread() {
	unsigned long remainingIterations = tNormalProgram.getIterations();
	LOG("Iterations remaining: %d", remainingIterations);
	switch(remainingIterations) {
		case 5:
			tDrain.restart();
			tNormalProgram.waitForDelayed(
				tDrain.getInternalStatusRequest(),
				tNormalProgram.getInterval(),
				tNormalProgram.getIterations()
			);
			break;
		case 4:
			tNormalProgram.waitForDelayed(
				runCycle(CycleType::PREWASH),
				tNormalProgram.getInterval(),
				tNormalProgram.getIterations()
			);
			break;
		case 3:
			tNormalProgram.waitForDelayed(
				runCycle(CycleType::MAIN_WASH),
				tNormalProgram.getInterval(),
				tNormalProgram.getIterations()
			);
			break;
		case 2:
			tNormalProgram.waitForDelayed(
				runCycle(CycleType::FINAL_WASH),
				tNormalProgram.getInterval(),
				tNormalProgram.getIterations()
			);
			break;
		case 1:
			tNormalProgram.waitForDelayed(
				runCycle(CycleType::DRY),
				tNormalProgram.getInterval(),
				tNormalProgram.getIterations()
			);
			break;
		case 0:
			LOG("Last iteration.");
			break;

		default:
			LOG("Unrecognized number of remaining iterations %d", remainingIterations);
			break;
	}
}

void normalProgramFinish() {
	LOG("Finished.");
    lastCommand = DishwasherCommand::NONE;
}

/////////////////
// Rinse program
#define RINSE_PROGRAM_ITERATIONS 5
void rinseProgramThread();
void rinseProgramFinish();

Task tRinseProgram(
	TASK_SECOND,
	RINSE_PROGRAM_ITERATIONS,
	rinseProgramThread,
	TS,
	false,
	NULL,
	rinseProgramFinish
);

void rinseProgramThread() {
	unsigned long remainingIterations = tRinseProgram.getIterations();
	LOG("Iterations remaining: %d", remainingIterations);
	switch(remainingIterations) {
		case 4:
			tDrain.restart();
			tRinseProgram.waitForDelayed(
				tDrain.getInternalStatusRequest(),
				tRinseProgram.getInterval(),
				tRinseProgram.getIterations()
			);
			break;
		case 3:
			// Since the FINAL_WASH cycle releases rinse aid, first we need to release detergent.
			tRinseProgram.waitForDelayed(
				releaseDetergent(),
				tRinseProgram.getInterval(),
				tRinseProgram.getIterations()
			);
			break;
		case 2:
			tRinseProgram.waitForDelayed(
				runCycle(CycleType::FINAL_WASH),
				tRinseProgram.getInterval(),
				tRinseProgram.getIterations()
			);
			break;
		case 1:
			tRinseProgram.waitForDelayed(
				runCycle(CycleType::DRY),
				tRinseProgram.getInterval(),
				tRinseProgram.getIterations()
			);
			break;
		case 0:
			LOG("Last iteration.");
			break;

		default:
			LOG("Unrecognized number of remaining iterations %d", remainingIterations);
			break;
	}
}

void rinseProgramFinish() {
	LOG("Finished.");
    lastCommand = DishwasherCommand::NONE;
}

///////////////
// Test program
#define TEST_PROGRAM_ITERATIONS 13
void testProgramThread();
void testProgramFinish();

Task tTestProgram(
	TASK_SECOND,
	TEST_PROGRAM_ITERATIONS,
	testProgramThread,
	TS,
	false,
	NULL,
	testProgramFinish
);

void testProgramThread() {
	unsigned long remainingIterations = tTestProgram.getIterations();
	LOG("Iterations remaining: %d", remainingIterations);
	switch(remainingIterations) {
		case 12:
			startFill();
			tTestProgram.setInterval(500 * TASK_MILLISECOND);
			break;
		case 11:
			stopFill();
			break;
		case 10:
			startDrain();
			break;
		case 9:
			stopDrain();
			break;

		case 8:
			startHeater();
			break;
		case 7:
			stopHeater();
			break;

		case 6:
			tTestProgram.waitForDelayed(
				startMainMotor(),
				tTestProgram.getInterval(),
				tTestProgram.getIterations()
			);
			break;
		case 5:
			stopMainMotor();
			break;

		case 4:
			startWaterSoftenerRegen();
			break;
		case 3:
			stopWaterSoftenerRegen();
			break;

		case 2:
			tTestProgram.waitForDelayed(
				releaseDetergent(),
				tTestProgram.getInterval(),
				tTestProgram.getIterations()
			);
			break;
		case 1:
			tTestProgram.waitForDelayed(
				releaseRinseAid(),
				tTestProgram.getInterval(),
				tTestProgram.getIterations()
			);
			break;
		case 0:
			LOG("Last iteration.");
			break;

		default:
			LOG("Unrecognized number of remaining iterations %d", remainingIterations);
			break;
	}
}

void testProgramFinish() {
	LOG("Finished.");
    lastCommand = DishwasherCommand::NONE;
    mqttClient.publish(MQTT_TOPIC_ROOT "status", 1, true, "idle");
    LOG("Published MQTT status update: idle");
}

/*
void dishwasherProgramThread() {
  // Loop forever
  for(;;) {
    mqttClient.publish(MQTT_TOPIC_ROOT "status", 1, true, "idle");
    Serial.println("[MQTT] Published status update: idle");

    PT_WAIT_UNTIL(pt, lastCommand != DishwasherCommand::NONE);

    mqttClient.publish(MQTT_TOPIC_ROOT "command", 1, true, dishwasherCommandNames[int8_t(lastCommand)]);
    Serial.print("[MQTT] Published command update: ");
    Serial.println(dishwasherCommandNames[int8_t(lastCommand)]);

    switch(lastCommand) {
      case DishwasherCommand::NORMAL_PROGRAM:
        mqttClient.publish(MQTT_TOPIC_ROOT "status", 1, true, "running");
        Serial.println("[MQTT] Published status update: running");

        ///////////
        mqttClient.publish(MQTT_TOPIC_ROOT "cycle", 1, true, "pre-wash");
        Serial.println("[Dishwasher] *** PRE-WASH CYCLE ***");

        Serial.println("[Dishwasher] Fill with water");
        mqttClient.publish(MQTT_TOPIC_ROOT "step", 1, true, "fill with water");
        digitalWrite(waterInletValve, HIGH);
        PT_SLEEP(pt, 20000);
        Serial.println("[Dishwasher] Done filling");
        mqttClient.publish(MQTT_TOPIC_ROOT "step", 1, true, "done with filling");
        digitalWrite(waterInletValve, LOW);

        PT_SLEEP(pt, 1000);

        Serial.println("[Dishwasher] Start main motor");
        mqttClient.publish(MQTT_TOPIC_ROOT "step", 1, true, "start main motor");
        digitalWrite(mainMotorStartup, HIGH);
        PT_SLEEP(pt, 500);
        digitalWrite(mainMotorRun, HIGH);
        PT_SLEEP(pt, 500);
        digitalWrite(mainMotorStartup, LOW);

        PT_SLEEP(pt, 10000);

        Serial.println("[Dishwasher] Start heater");
        mqttClient.publish(MQTT_TOPIC_ROOT "step", 1, true, "start heater");
        digitalWrite(heater, HIGH);
        PT_SLEEP(pt, 10000);
        Serial.println("[Dishwasher] Stop heater");
        mqttClient.publish(MQTT_TOPIC_ROOT "step", 1, true, "stop heater");
        digitalWrite(heater, LOW);

        PT_SLEEP(pt, 10000);

        Serial.println("[Dishwasher] Stop main motor");
        mqttClient.publish(MQTT_TOPIC_ROOT "step", 1, true, "stop main motor");
        digitalWrite(mainMotorRun, LOW);

        PT_SLEEP(pt, 1000);

        Serial.println("[Dishwasher] Drain water");
        mqttClient.publish(MQTT_TOPIC_ROOT "step", 1, true, "drain water");
        digitalWrite(drainMotor, HIGH);
        PT_SLEEP(pt, 20000);
        Serial.println("[Dishwasher] Done draining");
        mqttClient.publish(MQTT_TOPIC_ROOT "step", 1, true, "done draining");
        digitalWrite(drainMotor, LOW);

        //////////////////
        mqttClient.publish(MQTT_TOPIC_ROOT "cycle", 1, true, "main");
        Serial.println("[Dishwasher] *** MAIN WASH CYCLE ***");

        Serial.println("[Dishwasher] Fill with water");
        mqttClient.publish(MQTT_TOPIC_ROOT "step", 1, true, "fill with water");
        digitalWrite(waterInletValve, HIGH);
        PT_SLEEP(pt, 20000);
        Serial.println("[Dishwasher] Done filling");
        mqttClient.publish(MQTT_TOPIC_ROOT "step", 1, true, "done filling");
        digitalWrite(waterInletValve, LOW);

        PT_SLEEP(pt, 1000);

#if !ENABLE_ADC || !ENABLE_SERIAL
        Serial.println("[Dishwasher] Open soap dispenser");
        mqttClient.publish(MQTT_TOPIC_ROOT "step", 1, true, "open soap dispenser");
        digitalWrite(soapDispenserActuator, HIGH);
        PT_SLEEP(pt, 200);
        digitalWrite(soapDispenserActuator, LOW);
        PT_SLEEP(pt, 600);
        digitalWrite(soapDispenserActuator, HIGH);
        PT_SLEEP(pt, 600);
        digitalWrite(soapDispenserActuator, LOW);
#endif

        Serial.println("[Dishwasher] Start main motor");
        mqttClient.publish(MQTT_TOPIC_ROOT "step", 1, true, "start main motor");
        digitalWrite(mainMotorStartup, HIGH);
        PT_SLEEP(pt, 500);
        digitalWrite(mainMotorRun, HIGH);
        PT_SLEEP(pt, 500);
        digitalWrite(mainMotorStartup, LOW);

        PT_SLEEP(pt, 10000);

        Serial.println("[Dishwasher] Start heater");
        mqttClient.publish(MQTT_TOPIC_ROOT "step", 1, true, "start heater");
        digitalWrite(heater, HIGH);
        PT_SLEEP(pt, 10000);
        Serial.println("[Dishwasher] Stop heater");
        mqttClient.publish(MQTT_TOPIC_ROOT "step", 1, true, "stop heater");
        digitalWrite(heater, LOW);

        PT_SLEEP(pt, 10000);

        Serial.println("[Dishwasher] Stop main motor");
        mqttClient.publish(MQTT_TOPIC_ROOT "step", 1, true, "stop main motor");
        digitalWrite(mainMotorRun, LOW);

        PT_SLEEP(pt, 1000);

        Serial.println("[Dishwasher] Drain water");
        mqttClient.publish(MQTT_TOPIC_ROOT "step", 1, true, "drain water");
        digitalWrite(drainMotor, HIGH);
        PT_SLEEP(pt, 20000);
        Serial.println("[Dishwasher] Done draining");
        mqttClient.publish(MQTT_TOPIC_ROOT "step", 1, true, "done draining");
        digitalWrite(drainMotor, LOW);

#if 0
#if !ENABLE_ADC || !ENABLE_SERIAL
        Serial.println("[Dishwasher] Start water softener regen?");
        digitalWrite(waterSoftenerRegenValve, LOW);
#endif
#endif

        mqttClient.publish(MQTT_TOPIC_ROOT "status", 1, true, "finished");
        Serial.println("[MQTT] Published status update: finished");

        mqttClient.publish(MQTT_TOPIC_ROOT "step", 1, true, "");
        mqttClient.publish(MQTT_TOPIC_ROOT "cycle", 1, true, "");
        Serial.println("[Dishwasher] *** WASH CYCLES FINISHED ***");

        resetOutputs();

        // TODO: Wait for door switch before resetting to idle
        break;

      case DishwasherCommand::TEST_MAIN_MOTOR:
        Serial.println("[Dishwasher] Testing main motor");
        mqttClient.publish(MQTT_TOPIC_ROOT "startup", 1, true, "off");
        mqttClient.publish(MQTT_TOPIC_ROOT "run", 1, true, "off");

        for(int i = 0; i < 5; i++) {
          digitalWrite(mainMotorStartup, HIGH);
          mqttClient.publish(MQTT_TOPIC_ROOT "startup", 1, true, "on");
          PT_SLEEP(pt, 1000);

          digitalWrite(mainMotorRun, HIGH);
          mqttClient.publish(MQTT_TOPIC_ROOT "run", 1, true, "on");
          PT_SLEEP(pt, 1000);

          digitalWrite(mainMotorStartup, LOW);
          mqttClient.publish(MQTT_TOPIC_ROOT "startup", 1, true, "off");
          PT_SLEEP(pt, 1000);

          digitalWrite(mainMotorRun, LOW);
          mqttClient.publish(MQTT_TOPIC_ROOT "run", 1, true, "off");
          PT_SLEEP(pt, 1000);
        }

        break;

      case DishwasherCommand::TEST_HEATER:
        break;

      case DishwasherCommand::TEST_PINS:
        Serial.println("[Dishwasher] Testing problematic pins");

        resetOutputs();

        mqttClient.publish(MQTT_TOPIC_ROOT "IO13", 1, true, "off");
        mqttClient.publish(MQTT_TOPIC_ROOT "IO14", 1, true, "off");
        mqttClient.publish(MQTT_TOPIC_ROOT "IO16", 1, true, "off");
        mqttClient.publish(MQTT_TOPIC_ROOT "IO0", 1, true, "off");

        for(int i = 0; i < 5; i++) {
          digitalWrite(13, HIGH);
          mqttClient.publish(MQTT_TOPIC_ROOT "IO13", 1, true, "on");
          PT_SLEEP(pt, 1000);

          digitalWrite(13, LOW);
          mqttClient.publish(MQTT_TOPIC_ROOT "IO13", 1, true, "off");
          PT_SLEEP(pt, 1000);

          //////

          digitalWrite(14, HIGH);
          mqttClient.publish(MQTT_TOPIC_ROOT "IO14", 1, true, "on");
          PT_SLEEP(pt, 1000);

          digitalWrite(14, LOW);
          mqttClient.publish(MQTT_TOPIC_ROOT "IO14", 1, true, "off");
          PT_SLEEP(pt, 1000);

          //////

          digitalWrite(16, HIGH);
          mqttClient.publish(MQTT_TOPIC_ROOT "IO16", 1, true, "on");
          PT_SLEEP(pt, 1000);

          digitalWrite(16, LOW);
          mqttClient.publish(MQTT_TOPIC_ROOT "IO16", 1, true, "off");
          PT_SLEEP(pt, 1000);

          //////

          digitalWrite(0, HIGH);
          mqttClient.publish(MQTT_TOPIC_ROOT "IO0", 1, true, "on");
          PT_SLEEP(pt, 1000);

          digitalWrite(0, LOW);
          mqttClient.publish(MQTT_TOPIC_ROOT "IO0", 1, true, "off");
          PT_SLEEP(pt, 1000);
        }

        break;

      case DishwasherCommand::TEST_SOAP_DISPENSER:
        Serial.println("[Dishwasher] Testing soap dispenser");
        mqttClient.publish(MQTT_TOPIC_ROOT "soapDispenserTest", 1, true, "{}");
        Serial.println("[Dishwasher] published soapDispenserTest");

        DynamicJsonDocument doc(384);
        char docOut[512];

        for(int i = 0; i < 5; i++) {
          Serial.print("[Dishwasher] TEST_SOAP_DISPENSER iteration ");
          Serial.println(i);

          Serial.println("[Dishwasher] TEST_SOAP_DISPENSER waiting for button press...");
          PT_WAIT_UNTIL(pt, buttonState == LOW);
          Serial.println("[Dishwasher] ping");
          unsigned long startMillis = millis();

          Serial.println("[Dishwasher] Turning on soap dispenser actuator");
          digitalWrite(soapDispenserActuator, HIGH);

          PT_WAIT_UNTIL(pt, buttonState == HIGH);
          unsigned long phase1Millis = millis();
          doc[i]["phase1"] = phase1Millis - startMillis;
          serializeJson(doc, docOut);
          mqttClient.publish(MQTT_TOPIC_ROOT "soapDispenserTest", 1, true, docOut);

          Serial.println("[Dishwasher] Turning off soap dispenser actuator");
          digitalWrite(soapDispenserActuator, LOW);

          PT_WAIT_UNTIL(pt, buttonState == LOW);
          unsigned long phase2Millis = millis();
          doc[i]["phase2"] = phase2Millis - startMillis;
          serializeJson(doc, docOut);
          mqttClient.publish(MQTT_TOPIC_ROOT "soapDispenserTest", 1, true, docOut);

          Serial.println("[Dishwasher] Turning on soap dispenser actuator");
          digitalWrite(soapDispenserActuator, HIGH);

          PT_WAIT_UNTIL(pt, buttonState == HIGH);
          unsigned long phase3Millis = millis();
          doc[i]["phase3"] = phase3Millis - startMillis;
          serializeJson(doc, docOut);
          mqttClient.publish(MQTT_TOPIC_ROOT "soapDispenserTest", 1, true, docOut);

          Serial.println("[Dishwasher] Turning off soap dispenser actuator");
          digitalWrite(soapDispenserActuator, LOW);

          PT_WAIT_UNTIL(pt, buttonState == HIGH);
          unsigned long phase4Millis = millis();
          doc[i]["phase4"] = phase4Millis - startMillis;
          serializeJson(doc, docOut);
          mqttClient.publish(MQTT_TOPIC_ROOT "soapDispenserTest", 1, true, docOut);

          PT_WAIT_UNTIL(pt, buttonState == LOW);
        }

        break;
    }

    mqttClient.publish(MQTT_TOPIC_ROOT "command", 1, true, "");
    Serial.println("[MQTT] Published: emptied command");
    lastCommand = DishwasherCommand::NONE;
  }

  PT_END(pt);
}
*/


void executeCommand(DishwasherCommand command, const char* args) {
	uint8_t pin;
	lastCommand = command;
	switch(command) {
		case DishwasherCommand::NORMAL_PROGRAM:
			if(tNormalProgram.isEnabled()) {
				LOG("Ignoring NORMAL_PROGRAM command; task already enabled.");
			} else {
				tNormalProgram.setIterations(NORMAL_PROGRAM_ITERATIONS);
				tNormalProgram.restartDelayed(2 * TASK_SECOND);
				mqttClient.publish(MQTT_TOPIC_ROOT "status", 1, true, "running");
				mqttClient.publish(MQTT_TOPIC_ROOT "last_command", 1, true, "NORMAL_PROGRAM");
				LOG("Published MQTT update: status=running, last_command=NORMAL_PROGRAM");
			}
			break;
		case DishwasherCommand::RINSE_PROGRAM:
			if(tRinseProgram.isEnabled()) {
				LOG("Ignoring RINSE_PROGRAM command; task already enabled.");
			} else {
				tRinseProgram.setIterations(RINSE_PROGRAM_ITERATIONS);
				tRinseProgram.restartDelayed(2 * TASK_SECOND);
				mqttClient.publish(MQTT_TOPIC_ROOT "status", 1, true, "running");
				mqttClient.publish(MQTT_TOPIC_ROOT "last_command", 1, true, "RINSE_PROGRAM");
				LOG("Published MQTT update: status=running, last_command=RINSE_PROGRAM");
			}
			break;
		case DishwasherCommand::TEST_PINS:
			if(tTestProgram.isEnabled()) {
				LOG("Ignoring TEST_PINS command; task already enabled.");
			} else {
				tTestProgram.setIterations(TEST_PROGRAM_ITERATIONS);
				tTestProgram.restartDelayed(2 * TASK_SECOND);
				mqttClient.publish(MQTT_TOPIC_ROOT "status", 1, true, "running");
				mqttClient.publish(MQTT_TOPIC_ROOT "last_command", 1, true, "TEST_PINS");
				LOG("Published MQTT update: status=running, last_command=TEST_PINS");
			}
			break;
		case DishwasherCommand::START_MAIN_MOTOR:
			mqttClient.publish(MQTT_TOPIC_ROOT "status", 1, true, "running");
			mqttClient.publish(MQTT_TOPIC_ROOT "last_command", 1, true, "START_MAIN_MOTOR");
			LOG("Published MQTT update: status=running, last_command=START_MAIN_MOTOR");
			startMainMotor();
			break;
		case DishwasherCommand::STOP_MAIN_MOTOR:
			mqttClient.publish(MQTT_TOPIC_ROOT "status", 1, true, "running");
			mqttClient.publish(MQTT_TOPIC_ROOT "last_command", 1, true, "STOP_MAIN_MOTOR");
			LOG("Published MQTT update: status=running, last_command=STOP_MAIN_MOTOR");
			stopMainMotor();
			mqttClient.publish(MQTT_TOPIC_ROOT "status", 1, true, "idle");
			break;
		case DishwasherCommand::TURN_PIN_ON:
			mqttClient.publish(MQTT_TOPIC_ROOT "status", 1, true, "running");
			mqttClient.publish(MQTT_TOPIC_ROOT "last_command", 1, true, "TURN_PIN_ON");
			LOG("Published MQTT update: status=running, last_command=TURN_PIN_ON");
			LOG("Scanning args string:\"%s\" (length %u)", args, strlen(args));
			sscanf(args, "%uhh", &pin);
			LOG("Parsed unsigned char*: %u", pin);
			turnOnPin(pin);
			mqttClient.publish(MQTT_TOPIC_ROOT "status", 1, true, "idle");
			break;
		case DishwasherCommand::TURN_PIN_OFF:
			mqttClient.publish(MQTT_TOPIC_ROOT "status", 1, true, "running");
			mqttClient.publish(MQTT_TOPIC_ROOT "last_command", 1, true, "TURN_PIN_OFF");
			LOG("Published MQTT update: status=running, last_command=TURN_PIN_OFF");
			sscanf(args, "%uhh", &pin);
			turnOffPin(pin);
			mqttClient.publish(MQTT_TOPIC_ROOT "status", 1, true, "idle");
			break;
		default:
			LOG("Unrecognized command: %d", command);
			break;
	}
}

void executeCommand(const char* command, const char* args) {
	for(int8_t i; i < int8_t(DishwasherCommand::END); i++) {
		if(strcmp(command, dishwasherCommandNames[i]) == 0) {
			LOG("Received %s command.", command);
			// std::stringstream message;
			// message << "[MQTT] Received " << payload << " command.\n";
			// Serial.println(message.str().c_str());
			// mqttClient.publish(MQTT_TOPIC_ROOT "log", 1, true, message.str().c_str());
			executeCommand(DishwasherCommand(i), args);
			return;
		}
	}
	LOG("[MQTT] Received unrecognized command \"%s\"!", command);
}
