#ifndef __DISHWASHER_PROGRAM_HPP__
#define __DISHWASHER_PROGRAM_HPP__

#include "scheduler.hpp"

enum class DishwasherCommand : int8_t {
	NONE = 0,
	NORMAL_PROGRAM,
	RINSE_PROGRAM,
	TEST_MAIN_MOTOR,
	TEST_HEATER,
	TEST_PINS,
	TEST_SOAP_DISPENSER,
	START_MAIN_MOTOR,
	STOP_MAIN_MOTOR,
	TURN_PIN_ON,
	TURN_PIN_OFF,
	END
};

enum class CycleType : int8_t {
	PREWASH,
	MAIN_WASH,
	FINAL_WASH,
	FINAL_WASH_DELICATE,
	FINAL_WASH_SANITIZE,
	DRY,
	END
};

void executeCommand(DishwasherCommand command, const char* args = NULL);
void executeCommand(const char* command, const char* args = NULL);

#endif // __DISHWASHER_PROGRAM_HPP__
