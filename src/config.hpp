#ifndef __CONFIG_HPP__
#define __CONFIG_HPP__

#define MQTT_TOPIC_ROOT "devices/dishwasher/"

#define WIFI_RECONNECT_MIN_DELAY 1000
#define MQTT_RECONNECT_MIN_DELAY 6000

#ifdef LOLIN_S2_MINI
#  define ENABLE_SERIAL 1
#  define ENABLE_ADC 1
#  define ENABLE_WATER_SOFTENER_REGEN 1
#  define ENABLE_SOAP_DISPENSER 1
#else
#  define ENABLE_SERIAL 1
#  define ENABLE_ADC 0
#  if !ENABLE_ADC || !ENABLE_SERIAL
#    define ENABLE_WATER_SOFTENER_REGEN 1
#    define ENABLE_SOAP_DISPENSER 1
#  else
#    define ENABLE_WATER_SOFTENER_REGEN 0
#    define ENABLE_SOAP_DISPENSER 0
#  endif
#endif

#endif // __CONFIG_HPP__
