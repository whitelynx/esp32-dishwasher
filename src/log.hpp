#ifndef __LOG_HPP__
#define __LOG_HPP__

#include <cstring>
#include <stdio.h>
#include <stdarg.h>
#include <Arduino.h>
#include <AsyncMqttClient.h>

#include "config.hpp"


extern AsyncMqttClient mqttClient;

static bool suppressLog = false;

#define LOG(MSG, ...) \
	_log(__FILE__, __LINE__, __FUNCTION__, MSG, ##__VA_ARGS__)

inline void _log(const char* file, int line, const char* function, const char* format, ...) {
	if(suppressLog) {
		return;
	}

	va_list args;
	va_start(args, format);

	char buf[1024];

	size_t file_offset = strlen(file) - 1;
	while(file[file_offset] != '/' && file_offset > 0) {
		file_offset--;
	}

	size_t written = snprintf(buf, 1024, "%u [%s:%d:%s] ", millis(), file + file_offset + 1, line, function);
	vsnprintf(buf + written, 1024 - written, format, args);

#if ENABLE_SERIAL
	Serial.println(buf);
#endif
	mqttClient.publish(MQTT_TOPIC_ROOT "log", 1, true, buf);

	va_end(args);
}

#endif // __LOG_HPP__
