#include <Arduino.h>
#include <ArduinoJson.h>
#include <AsyncMqttClient.h>
#include <WiFi.h>
#include <sstream>

#include "config.hpp"
#include "secrets.h"
#include "scheduler.hpp"
#include "Dishwasher.hpp"
#include "DishwasherProgram.hpp"

const uint8_t ntcNumSamples = 8;

const char *asyncMqttClientDisconnectReasonNames[] = {
  "TCP_DISCONNECTED", // = 0

  "MQTT_UNACCEPTABLE_PROTOCOL_VERSION", // = 1
  "MQTT_IDENTIFIER_REJECTED", // = 2
  "MQTT_SERVER_UNAVAILABLE", // = 3
  "MQTT_MALFORMED_CREDENTIALS", // = 4
  "MQTT_NOT_AUTHORIZED", // = 5

  "ESP8266_NOT_ENOUGH_SPACE", // = 6

  "TLS_BAD_FINGERPRINT" // = 7
};

bool wifiConnected = false;
bool mqttConnected = false;
int buttonState = HIGH;

StatusRequest reconnectToWifi;
StatusRequest reconnectToMqtt;

AsyncMqttClient mqttClient;


////////
// Tasks
#define BLINK_PERIOD_CONNECTED 200
#define BLINK_PERIOD_DISCONNECTED 700
void blinkLED();
Task tBlink(
	BLINK_PERIOD_DISCONNECTED * TASK_MILLISECOND,
	TASK_FOREVER,
	&blinkLED,
	TS,
	true
);

void reconnectWifi();
Task tReconnectWifi(
	WIFI_RECONNECT_MIN_DELAY * TASK_MILLISECOND,
	TASK_ONCE,
	reconnectWifi,
	TS,
	false
);

void reconnectMqtt();
Task tReconnectMqtt(
	MQTT_RECONNECT_MIN_DELAY * TASK_MILLISECOND,
	TASK_ONCE,
	reconnectMqtt,
	TS,
	false
);

void startCycle();
Task tStartCycle(
	TASK_SECOND,
	TASK_ONCE,
	startCycle,
	TS,
	false
);


///////////////////////////
// WiFi connection handling
void connectToWifi() {
  LOG("[WiFi] Connecting to %s...", WIFI_SSID);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
}

void reconnectWifi() {
  connectToWifi();

  reconnectToWifi.setWaiting();
  tReconnectWifi.waitForDelayed(&reconnectToWifi, WIFI_RECONNECT_MIN_DELAY * TASK_MILLISECOND);
}

void WiFiEvent(WiFiEvent_t event) {
  LOG("[WiFi] event: %d", event);

  switch(event) {
	  case SYSTEM_EVENT_STA_GOT_IP: {
		  wifiConnected = true;
		  tBlink.setInterval(0);
		  IPAddress ourIP = WiFi.localIP();
		  LOG("Connected; IP address: %u.%u.%u.%u", ourIP[0], ourIP[1], ourIP[2], ourIP[3]);
		  tReconnectMqtt.enable();
		  reconnectToMqtt.setWaiting();
		  tReconnectMqtt.waitForDelayed(&reconnectToMqtt, MQTT_RECONNECT_MIN_DELAY * TASK_MILLISECOND);
		  reconnectToMqtt.signal();
		  break;
	  }

	  case SYSTEM_EVENT_STA_DISCONNECTED:
		  wifiConnected = false;
		  mqttConnected = false;
		  tBlink.setInterval(0);
		  LOG("[WiFi] lost connection");
		  tReconnectMqtt.disable(); // ensure we don't reconnect to MQTT while reconnecting to Wi-Fi
		  reconnectToWifi.signal();
		  break;
  }
}

///////////////////////////
// MQTT connection handling

void connectToMqtt() {
  LOG("Connecting to %u.%u.%u.%u:%u...", MQTT_HOST[0], MQTT_HOST[1], MQTT_HOST[2], MQTT_HOST[3], MQTT_PORT);
  mqttClient.connect();
  reconnectToMqtt.setWaiting();
}

void reconnectMqtt() {
    connectToMqtt();

  reconnectToMqtt.setWaiting();
  tReconnectMqtt.waitForDelayed(&reconnectToMqtt, MQTT_RECONNECT_MIN_DELAY * TASK_MILLISECOND);
}

void onMqttConnect(bool sessionPresent) {
  reconnectToMqtt.setWaiting();

  mqttConnected = true;
  tBlink.setInterval(0);
  LOG("MQTT connected.");
  LOG("  Session present: %s", sessionPresent ? "true" : "false");
  uint16_t packetIdSub = mqttClient.subscribe(MQTT_TOPIC_ROOT "command", 2);
  LOG("  Subscribing at QoS 2, packetId: %u", packetIdSub);
  mqttClient.publish(MQTT_TOPIC_ROOT "status", 1, true, "online");
  LOG("  Published status update: online");
}

void onMqttDisconnect(AsyncMqttClientDisconnectReason reason) {
  mqttConnected = false;
  tBlink.setInterval(0);
  LOG("MQTT disconnected; reason: %s", asyncMqttClientDisconnectReasonNames[int8_t(reason)]);

  if(WiFi.isConnected()) {
    tReconnectMqtt.enable();
    reconnectToMqtt.setWaiting();
    tReconnectMqtt.waitForDelayed(&reconnectToMqtt, MQTT_RECONNECT_MIN_DELAY * TASK_MILLISECOND);
    reconnectToMqtt.signal();
  }
}

void onMqttSubscribe(uint16_t packetId, uint8_t qos) {
  Serial.println("[MQTT] Subscribe acknowledged.");
  Serial.print("  packetId: ");
  Serial.println(packetId);
  Serial.print("  qos: ");
  Serial.println(qos);
}

void onMqttUnsubscribe(uint16_t packetId) {
  Serial.println("[MQTT] Unsubscribe acknowledged.");
  Serial.print("  packetId: ");
  Serial.println(packetId);
}

void onMqttMessage(char *topic, char *payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index,
                   size_t total) {
  Serial.println("[MQTT] Publish received.");
  Serial.print("  topic: ");
  Serial.println(topic);
  Serial.print("  qos: ");
  Serial.println(properties.qos);
  Serial.print("  dup: ");
  Serial.println(properties.dup);
  Serial.print("  retain: ");
  Serial.println(properties.retain);
  Serial.print("  len: ");
  Serial.println(len);
  Serial.print("  index: ");
  Serial.println(index);
  Serial.print("  total: ");
  Serial.println(total);

  char payloadBuf[len + 1];
  payloadBuf[len] = 0;
  strncpy(payloadBuf, payload, len);

  if(strcmp(topic, MQTT_TOPIC_ROOT "command") == 0) {
	// Check if there's args for the command
	char *sep = strstr(payloadBuf, " ");
	const char *args = 0;
	if(sep != NULL) {
	  sep[0] = 0;
	  args = sep + 1;
	}

	executeCommand(payloadBuf, args);
  }
}

void onMqttPublish(uint16_t packetId) {
  //Serial.println("[MQTT] Publish acknowledged.");
  //Serial.print("  packetId: ");
  //Serial.println(packetId);
}

//////////////////
// Button handling
void startCycle() {
	if(isStartButtonPressed()) {
		LOG("Starting NORMAL_PROGRAM program...");
		executeCommand(DishwasherCommand::NORMAL_PROGRAM);
		tStartCycle.waitFor(getStartButtonStatusRequest());
	}
}

//////////////////////////////////////
// Read water temperature periodically
#if ENABLE_ADC
#define READ_TEMPERATURE_PERIOD 10000
void readTemperature();
Task tReadTemperature(
	READ_TEMPERATURE_PERIOD * TASK_MILLISECOND,
	TASK_FOREVER,
	&readTemperature,
	TS,
	true
);

void readTemperature() {
	readWaterTemperature();
}
#endif

////////
// Blink
bool ledState = false;
void blinkLED() {
  suppressLog = true;
  if(ledState) {
	turnOffLED();
  } else {
	turnOnLED();
  }
  suppressLog = false;
  ledState = !ledState;
  if(mqttConnected) {
	if(ledState) {
	  tBlink.setInterval(BLINK_PERIOD_DISCONNECTED * 2 * TASK_MILLISECOND);
	} else {
	  tBlink.setInterval(BLINK_PERIOD_CONNECTED / 2 * TASK_MILLISECOND);
	}
  } else if(wifiConnected) {
	tBlink.setInterval(BLINK_PERIOD_CONNECTED * TASK_MILLISECOND);
  } else {
	tBlink.setInterval(BLINK_PERIOD_DISCONNECTED * TASK_MILLISECOND);
  }
}

//////////////////////
// Setup and main loop

void setup() {
	setupPins();

	resetOutputs();

#if ENABLE_SERIAL
	Serial.begin(115200);
	sleep(5);
	LOG("");
	LOG("");
	LOG("Setting up...");
#endif

	WiFi.onEvent(WiFiEvent);

	mqttClient.onConnect(onMqttConnect)
		.onDisconnect(onMqttDisconnect)
		.onSubscribe(onMqttSubscribe)
		.onUnsubscribe(onMqttUnsubscribe)
		.onMessage(onMqttMessage)
		.onPublish(onMqttPublish)

		.setServer(MQTT_HOST, MQTT_PORT)
		.setCredentials(MQTT_USERNAME, MQTT_PASSWORD)
		.setClientId("dishwasher")
		.setWill(MQTT_TOPIC_ROOT "status", 0, true, "offline");

	connectToWifi();
	// TODO: Set up OTA updates

	LOG("Listening for button press...");
	tStartCycle.waitFor(getStartButtonStatusRequest());

	LOG("Setup finished.");
}

void loop() {
	TS->execute();
}
